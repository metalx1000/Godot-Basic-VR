extends Spatial


var right_pinching = false
var left_pinching = false

var initial_radius = 0
var initial_transform = 0

var pinch_start_transform
var left_pinch_start_transform
var right_hand
var left_hand

var previous_distance = 0
var previous_scale_distance = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()


func _on_ARVROriginWithHandTrackingCustom_right_index_pinching(hand):
	get_tree().get_nodes_in_group("cubes")[0].ro = true
	right_pinching = true
	right_hand = hand


func _on_ARVROriginWithHandTrackingCustom_right_index_released(hand):
	get_tree().get_nodes_in_group("cubes")[0].ro = false
	right_pinching = false
	right_hand = null



