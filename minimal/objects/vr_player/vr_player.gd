extends KinematicBody

var up = Vector3(0,1,0)
var direction = Vector3()
var velocity = Vector3()
var speed = 5
var gravity_vec = Vector3()                                                                                                                                               
var movement = Vector3()
var snap = Vector3.DOWN
var accel = 7

onready var target = $target

onready var eye_1 = $HBoxContainer/eye_1/Viewport/head/Camera
onready var eye_2 = $HBoxContainer/eye_2/Viewport/head/Camera
onready var head = [$HBoxContainer/eye_1/Viewport/head,$HBoxContainer/eye_2/Viewport/head]

var gyro = Input.get_gyroscope()
var acc = Input.get_accelerometer()
var mag = Input.get_magnetometer()
var sensors = [Input.get_gyroscope(),
	Input.get_accelerometer(),
	Input.get_magnetometer()]
	
var count = 0.0001
func _ready():
	var look = target.transform.origin
	eye_1.look_at(look,up)
	eye_2.look_at(look,up)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	sensor_display()
	
	walk(delta)
	move(delta)
	#transform.origin.x += 1 * delta
	var dir = Input.get_action_strength("ui_left") - Input.get_action_strength("ui_right")
	rotate(Vector3(0,1,0), dir * delta)
#	
	
	head[0].rotation = rotation
	head[1].rotation = rotation
		
	head[0].transform.origin = transform.origin
	head[1].transform.origin = transform.origin
	#var look = target.transform.origin
	#look_at(look,up)
	#var look = target.transform.origin
	#eye_1.look_at(look,up)
	#eye_2.look_at(look,up)
	#rotation.y += 1
	
func walk(delta):
		#get keyboard input
	direction = Vector3.ZERO
	var h_rot = global_transform.basis.get_euler().y
	var f_input = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	#var h_input = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	var h_input = 0
	direction = Vector3(h_input, 0, f_input).rotated(Vector3.UP, h_rot).normalized()
	
func move(delta):    
	velocity = velocity.linear_interpolate(direction * speed, accel * delta)
	movement = velocity + gravity_vec
	move_and_slide_with_snap(movement, snap, Vector3.UP) 

func sensor_display():
	var output = ""
	for s in sensors:
		output = output + str(s)
	
	$HBoxContainer/eye_1/Control/Label.text = output
	$HBoxContainer/eye_2/Control2/Label.text = output
	
func t(event):
	var output = event.to_string()
	$HBoxContainer/eye_1/Control/Label.text = output
	$HBoxContainer/eye_2/Control2/Label.text = output
	
